// Constants
const PORT = process.env.PORT || 3000;

// Configure the server
const express = require('express');
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(cors());


// Database connection
const Pool = require('pg').Pool;
const pool = new Pool({
    user: 'admin',
    password: '2AbVYyfU9Lb0IMVCkusZkVQmjfcXBWaV',
    host: 'dpg-coo0pru3e1ms73ftmegg-a.oregon-postgres.render.com',
    port: 5432, // default Postgres port
    database: 'latinad',
    ssl: {
        rejectUnauthorized: false
    }
});
function query(text, params) { return pool.query(text, params); }

//Helpers
function encodeToBase64(str) {
    return Buffer.from(str).toString('base64');
}

function decodeFromBase64(encodedStr) {
    return Buffer.from(encodedStr, 'base64').toString('utf-8');
}

async function getUserIdFromToken(req, res) {
    try {
        const token = req.headers.authorization.replace("Bearer ","");
        const decoded = decodeFromBase64(token);
        const parts = decoded.split("-");
        const user = {
            id: parts[0],
            email: parts[1]
        };
        // find user in db
        const userInDb = await query('SELECT * FROM users WHERE email = $1 AND id = $2', [user.email, user.id]);
        if (userInDb.rows.length === 0) {
            res.status(401).send('Unauthorized');
        }
        return user.id;

    } catch (err) {
        console.error(err);
        res.status(401).send('Error parsing token');
        return -1;
    }

}

function getDisplayImage (type) {
    if (type === 'outdoor') {
        return "https://5.imimg.com/data5/SELLER/Default/2022/8/EU/EM/YX/138202095/p10-outdoor-full-color-led-display-500x500.png";
    } else {
        return "https://res.cloudinary.com/dbihouiij/image/upload/c_scale,dpr_auto,f_auto,w_auto/v1/SiteImages/0000495_49-air-conditioned-all-weather-display";
    }
}

// ENDPOINTS


// Login
app.post('/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        const result = await query('SELECT * FROM users WHERE email = $1 AND password = $2', [email, password]);
        if (result.rows.length === 0) {
            res.status(401).send('Incorrect user or password');
        } else {
            const response = {
                name: result.rows[0].full_name,
                email: result.rows[0].email,
                token: encodeToBase64(result.rows[0].id + "-" + result.rows[0].email)
            }
            res.json(response);
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Error on login');
    }
});

// List displays
app.get('/display', async (req, res) => {
    try {
        const userId = await getUserIdFromToken(req, res);
        const pageSize = parseInt(req.query.pageSize);
        const offset = parseInt(req.query.offset) || 0; // Default offset to 0 if not provided
        if (isNaN(pageSize) || pageSize <= 0 || isNaN(offset) || offset < 0) {
            return res.status(400).send('Invalid page size or offset');
        }

        let queryText = 'SELECT * FROM displays WHERE user_id = $1';
        const queryParams = [userId];

        if (req.query.type) {
            queryText += ' AND type = $' + (queryParams.length + 1);
            queryParams.push(req.query.type);
        }

        if (req.query.name) {
            queryText += ' AND name ILIKE $' + (queryParams.length + 1);
            queryParams.push('%' + req.query.name + '%');
        }

        let countQueryText = 'SELECT COUNT(*) FROM displays WHERE user_id = $1';
        let countQueryParams = [userId];

        if (req.query.type) {
            countQueryText += ' AND type = $' + (countQueryParams.length + 1);
            countQueryParams.push(req.query.type);
        }

        if (req.query.name) {
            countQueryText += ' AND name ILIKE $' + (countQueryParams.length + 1);
            countQueryParams.push('%' + req.query.name + '%');
        }

        const countResult = await query(countQueryText, countQueryParams);
        const totalCount = parseInt(countResult.rows[0].count);

        queryText += ' LIMIT $' + (queryParams.length + 1);
        queryParams.push(pageSize);

        queryText += ' OFFSET $' + (queryParams.length + 1);
        queryParams.push(offset);

        const result = await query(queryText, queryParams);
        res.json({ totalCount, data: result.rows });
    } catch (err) {
        console.error(err);
        res.status(500).send('Error listing displays');
    }
});

app.get('/display/:id', async (req, res) => {
    try {
        const userId = await getUserIdFromToken(req, res);
        const result = await query('SELECT * FROM displays WHERE id = $1 AND (user_id IS NULL OR user_id = $2)', [req.params.id, userId]);
        if (result.rows.length === 0) {
            res.status(404).send('Not Found');
        } else {
            res.json(result.rows[0]);
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Error getting display');
    }
});

app.post('/display', async (req, res) => {
    try {
        const userId = await getUserIdFromToken(req, res);
        const { name, description, price_per_day, resolution_height, resolution_width, type } = req.body;
        // Check that all values are present
        if (!name || !description || !price_per_day || !resolution_height || !resolution_width || !type) {
            return res.status(400).send('Missing one or more required fields: name, description, price_per_day, resolution_height, resolution_width, type');
        }
        const picture_url = getDisplayImage(type);
        const result = await query('INSERT INTO displays (name, description, user_id, price_per_day, resolution_height, resolution_width, type, picture_url) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *', [name, description, userId, price_per_day, resolution_height, resolution_width, type, picture_url]);
        res.json(result.rows[0]);
    } catch (err) {
        console.error(err);
        res.status(500).send('Error creating display');
    }
});

app.put('/display/:id', async (req, res) => {
    try {
        const userId = await getUserIdFromToken(req, res);
        const { name, description, price_per_day, resolution_height, resolution_width, type } = req.body;
        // Check that all values are present
        if (!name || !description || !price_per_day || !resolution_height || !resolution_width || !type) {
            return res.status(400).send('Missing one or more required fields: name, description, price_per_day, resolution_height, resolution_width, type');
        }
        const picture_url = getDisplayImage(type);
        const result = await query('UPDATE displays SET name = $1, description = $2, price_per_day = $3, resolution_height = $4, resolution_width = $5, type = $6, picture_url = $7 WHERE id = $8 AND user_id = $9 RETURNING *', [name, description, price_per_day, resolution_height, resolution_width, type, picture_url, req.params.id, userId]);
        if (result.rows.length === 0) {
            res.status(404).send('Not Found');
        } else {
            res.json(result.rows[0]);
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Error updating display');
    }
});

app.delete('/display/:id', async (req, res) => {
    try {
        const userId = await getUserIdFromToken(req, res);
        const result = await query('DELETE FROM displays WHERE id = $1 AND user_id = $2 RETURNING *', [req.params.id, userId]);
        if (result.rows.length === 0) {
            res.status(404).send('Not Found');
        } else {
            res.json(result.rows[0]);
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Error deleting display');
    }
});





// Start app
app.listen(PORT, () => {
    console.log("Server Listening on PORT:", PORT);
});